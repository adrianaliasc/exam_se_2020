package subiect02;

public class Main {
    public static void main(String[] args) {
        /*
        Three separate thread classes, 3 separate threads running intermittently (due to the sleep
        function), each printing out the count they're on (for clarity), the thread name (sent through
        an argument) and the current, local time (to see how much time I have left for the exam).
         */
        UThread01 t1 = new UThread01("UThread01");
        UThread02 t2 = new UThread02("UThread02");
        UThread03 t3 = new UThread03("UThread03");

        t1.start();
        t2.start();
        t3.start();
    }
}

class UThread01 extends Thread {
    public UThread01(String name) {
        super(name);
    }

    @Override
    public void run() {
        super.run();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < Util.DURATION; i++) {
            System.out.println(i + ". " + this.getName() + "-" + java.time.LocalTime.now());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}

class UThread02 extends Thread {
    public UThread02(String name) {
        super(name);
    }

    @Override
    public void run() {
        super.run();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < Util.DURATION; i++) {
            System.out.println(i + ". " + this.getName() + "-" + java.time.LocalTime.now());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


class UThread03 extends Thread {
    public UThread03(String name) {
        super(name);
    }

    @Override
    public void run() {
        super.run();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < Util.DURATION; i++) {
            System.out.println(i + ". " + this.getName() + "-" + java.time.LocalTime.now());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Util {
    public static int DURATION = 25;
}

