package subiect01;

/*

 */

public class Main {
    public static void main(String[] args) {
        // Apparently not necessary.
        J theJ = new J();
        N theN = new N();
        I theI = new I(50, theN);
        theI.f();
        theI.i(theJ);
    }
}

class I {
    // Aggregation here,  some dependency and association.
    private long t;
    private N n;
    private K k;

    public I(long t, N n) {
        this.t = t;
        this.n = n;
    }

    // Association, kinda.
    public void setK(K k) {
        this.k = k;
    }

    public void f() {
        System.out.println("Help");
    }

    // This function is dependent on J to work.
    public void i(J j){
        System.out.println("Help, but with J and I am dependent on it.");
    }

}

class S{
    public void metB(){
        System.out.println("MethodB, oh yeah!");
    }
}

class L {
    public void metA(){
        System.out.println("The A method, yeee!!!");
    }
}

class K extends S{
    // L composition here
    L theL;

    public K(L theL) {
        this.theL = new L();
    }
}

class N {
}

class J {
}

